<?php
require('../db_connect.php');
header('Content-type:application/json;charset=utf-8');
/* Requête "Select" retourne un jeu de résultats */
if ($result = $link->query("SELECT * FROM Outils")) {
    if($result){
         // Cycle through results
        while ($row = $result->fetch_array()){
            $tools_arr[] = $row;
        }
        // Free result set
        $result->close();
    }
    /* Libération du jeu de résultats */
    $result->close();
    $out = array_values($tools_arr);
    echo json_encode($out);
}

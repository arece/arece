import { BrowserModule } from '@angular/platform-browser';
import { NgModule,ModuleWithProviders } from '@angular/core';
import { MatButtonModule, MatCardModule, MatMenuModule, MatToolbarModule, MatIconModule, MatInputModule, MatDatepickerModule, MatNativeDateModule, MatProgressSpinnerModule,
    MatTableModule, MatExpansionModule, MatSelectModule, MatSnackBarModule, MatTooltipModule, MatChipsModule, MatListModule, MatSidenavModule, MatTabsModule, MatProgressBarModule
} from '@angular/material';
import { MatFormFieldModule } from '@angular/material';
import {MatRadioModule} from '@angular/material/radio';
import {MatDialogModule} from '@angular/material/dialog';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes, CanActivate} from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {MatStepperModule} from '@angular/material/stepper';

import { AppComponent } from './Components/Component/app.component';
import { Header } from './Components/Header/header';
import { Footer } from './Components/Footer/footer';
import { Accueil } from './Components/Accueil/accueil';
import { About } from './Components/About/about';
import { Contact } from './Components/Contact/contact';
import { Documentation } from './Components/Documentation/documentation';
import { DialogDiagnosol } from './Components/Documentation/documentation';
import { DialogCiesst } from './Components/Documentation/documentation';
import { DialogCalsimeol } from './Components/Documentation/documentation';
import { DialogEolAtlas } from './Components/Documentation/documentation';
import { Diagnosol } from './Components/Diagnosol/diagnosol';
import { Ciesst } from './Components/Ciesst/ciesst';
import { Calsimeol } from './Components/Calsimeol/calsimeol';
import { Forum } from './Components/Forum/forum';
import { Administration } from './Components/Administration/administration';
import { AuthGuard } from './Components/Administration/administration';
import { ToolsList } from './Components/Administration/ToolsList/toolsList';
import { Tool } from './Components/Objects/Tool/tool';
import { ToolService } from './Components/Objects/Tool/toolservice';
import { ToolForm } from './Components/Objects/Tool/form/tool-form';
import { ToolView } from './Components/ToolView/tool-view';
import { Utilisateur } from './Components/Objects/Utilisateur/utilisateur';
import { UtilisateurService } from './Components/Objects/Utilisateur/utilisateur.service';
import { Connexion } from './Components/Objects/Connexion/connexion';
import { NotFoundComponent } from './Components/Not-Found/not-found';



const appRoutes: Routes = [
  {path: 'accueil-arece', component: Accueil},
  {path: 'documentation-outils', component: Documentation},
  {path: 'about-arece', component: About},
  {path: 'contact-arece', component: Contact},
  {path: 'diagnosol', component: Diagnosol},
  {path: 'ciesst', component: Ciesst},
  {path: 'calsimeol', component: Calsimeol},
  {path: 'forum-arece', component: Forum},
  {path: 'addtool-arece', component: ToolForm},
  {path: 'tool-view/:id', component: ToolView},
  {path: 'administration-arece', canActivate : [AuthGuard], component: Administration, children:[
    {path: ':adminlist', outlet:'adminPanel', component: ToolsList}
  ]},
  {path: 'connexion-admin-arece', component: Connexion},
  { path: '', component: Accueil },
  { path: '**', component: NotFoundComponent }
];


@NgModule({
  declarations: [
    AppComponent,Header,Footer,
    Accueil,Documentation,About,Contact,Forum,
    Diagnosol,Ciesst,Calsimeol,Administration,ToolsList,ToolForm,ToolView,
    DialogDiagnosol,DialogCiesst,DialogCalsimeol,DialogEolAtlas,
    Connexion,Utilisateur,NotFoundComponent
  ],
  imports: [
    BrowserModule,HttpModule,MatTableModule,MatStepperModule,MatMenuModule,
    FormsModule,ReactiveFormsModule,MatButtonModule,MatInputModule,MatFormFieldModule,MatSnackBarModule,
    MatCardModule,NoopAnimationsModule,MatRadioModule,MatDialogModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ], entryComponents: [
    DialogDiagnosol,DialogCiesst,DialogCalsimeol,DialogEolAtlas
  ],
  providers: [
    AuthGuard
  ],
  bootstrap: [AppComponent,Header]
})
export class AppModule { }

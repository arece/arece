import { Component, ViewChild, Inject, OnInit } from '@angular/core';
import { Meta, Title} from '@angular/platform-browser';

import {MatDialog, MatDialogRef,MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-documentation',
  templateUrl: './documentation.html',
  styleUrls: ['./documentation.css']
})
export class Documentation{
  title = 'Documentation';

constructor(public dialog: MatDialog, meta: Meta, title: Title) {

      title.setTitle('Documentation des outils de dimensionnement d\'énergie');

      meta.addTags([
        { name: 'author',   content: 'ECE Paris'},
        { name: 'keywords', content: 'plateforme ARECE, dimensionnement énergie, documentation outils'},
        { name: 'description', content: 'Sont recensés tous les documents liés aux outils de dimensionnement d\'énergie Ciesst Diagnosol EolAtlas Calsimeol disponibles sur la plateforme ARECE' }
      ]);
}

  openDiagnosol(): void {
    let dialogRef = this.dialog.open(DialogDiagnosol,{
      width: '1000px',
      height: '600px'
    });
  }
  openCiesst(): void {
    let dialogRef = this.dialog.open(DialogCiesst,{
      width: '1000px',
      height: '600px'
    });
  }
  openCalsimeol(): void {
      let dialogRef = this.dialog.open(DialogCalsimeol,{
        width: '1000px',
        height: '600px'
      });
  }
  openEolAtlas(): void {
      let dialogRef = this.dialog.open(DialogEolAtlas,{
        width: '1000px',
        height: '600px'
      });
  }



}






@Component({
  templateUrl: 'dialog-diagnosol-doc.html',
  styleUrls: ['./dialog-tool-doc.css']
})
export class DialogDiagnosol {
  constructor(
    public dialogRef: MatDialogRef<DialogDiagnosol>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
@Component({
  templateUrl: 'dialog-calsimeol-doc.html',
  styleUrls: ['./dialog-tool-doc.css']
})
export class DialogCalsimeol {
  constructor(
    public dialogRef: MatDialogRef<DialogCalsimeol>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
@Component({
  templateUrl: 'dialog-ciesst-doc.html',
  styleUrls: ['./dialog-tool-doc.css']
})
export class DialogCiesst {
  constructor(
    public dialogRef: MatDialogRef<DialogCiesst>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

@Component({
  templateUrl: 'dialog-eolatlas-doc.html',
  styleUrls: ['./dialog-tool-doc.css']
})
export class DialogEolAtlas {
  constructor(
    public dialogRef: MatDialogRef<DialogEolAtlas>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

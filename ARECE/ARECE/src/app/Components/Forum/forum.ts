import { Component, ViewChild } from '@angular/core';
import { Meta, Title} from '@angular/platform-browser';


@Component({
  selector: 'forum',
  templateUrl: './forum.html',
  styleUrls: ['./forum.css']
})
export class Forum {
  constructor(meta: Meta, title: Title) {

  title.setTitle('Forum ARECE');

  meta.addTags([
    { name: 'author',   content: 'ECE Paris'},
    { name: 'keywords', content: 'plateforme ARECE, dimensionnement énergie, Forum énergie'},
    { name: 'description', content: 'Forum de discussion et d\'entraide pour les différents utilisateurs de la plalteforme sur les outils de dimensionnement d\'énergie proposés par la plateforme ARECE' }
  ]);}

}

import { Component,OnInit, ViewChild } from '@angular/core';
import {Router} from '@angular/router';
import {Tool} from '../Objects/Tool/tool';
import {ToolService} from '../Objects/Tool/toolservice';

@Component({
  selector: 'app-header',
  templateUrl: './header.html',
  styleUrls: ['./header.css'],
  providers:[ToolService]
})
export class Header implements OnInit {
  tools:Tool[];

  constructor(private toolService:ToolService,private router:Router){

  }

  ngOnInit() {
    this.toolService.getTools().subscribe((tools: Tool[]) => {
        this.tools = tools;
    },
    // Errors will call this callback instead:
    err => {
      console.log('Something went wrong!');
    });

  }

  refreshList(){
    this.toolService.getTools().subscribe((tools: Tool[]) => {
        this.tools = tools;
    },
    // Errors will call this callback instead:
    err => {
      console.log('Something went wrong!');
    });

  }
  goToTool(id:number){
    this.router.navigate(['/tool-view',id]);
  }

}

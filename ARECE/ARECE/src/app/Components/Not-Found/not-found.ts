import { Component, OnInit } from '@angular/core';
import { Meta, Title} from '@angular/platform-browser';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.html',
  styleUrls: ['./not-found.css']
})
export class NotFoundComponent{

  constructor(meta: Meta, title: Title) {

        title.setTitle('Not found');

        meta.addTags([
          { name: 'author',   content: 'ECE Paris'},
          { name: 'keywords', content: 'plateforme ARECE, dimensionnement énergie, not Found'},
          { name: 'description', content: 'Page 404, lorsque l\'url renseigné n\'est pas correct cette page est affichée' }
        ]);
  }

}

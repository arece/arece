import { Component, ViewChild } from '@angular/core';
import { Meta, Title} from '@angular/platform-browser';


@Component({
  selector: 'app-ciesst',
  templateUrl: './ciesst.html',
  styleUrls: ['./ciesst.css']
})
export class Ciesst {
  title = 'header';

  constructor(meta: Meta, title: Title) {
  title.setTitle('Outil Ciesst');

  meta.addTags([
    { name: 'author',   content: 'ECE Paris'},
    { name: 'keywords', content: 'plateforme ARECE, dimensionnement énergie, Ciesst'},
    { name: 'description', content: '' }
  ]);
}
}

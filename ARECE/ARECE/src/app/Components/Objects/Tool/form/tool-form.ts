import { Component, Inject } from '@angular/core';
import {Jsonp} from '@angular/http';

import {Tool} from '../tool';
import {MatSnackBar} from '@angular/material';

import {ToolService} from '../toolservice';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import { Meta, Title} from '@angular/platform-browser';



@Component({
  selector: 'app-tool-form',
  templateUrl: './tool-form.html',
  styleUrls: ['./tool-form.css'],
  providers: [ToolService]
})
export class ToolForm {
  tool:Tool;
  form: FormGroup;
  errorMessage: any;
  message : string = "";
  success:boolean;
  types = [
   'Hébergé',
   'Non hébergé'
 ];
   constructor(@Inject(FormBuilder) fb: FormBuilder, private toolService: ToolService,public snackBar: MatSnackBar, meta: Meta, title: Title) {
     title.setTitle('Formulaire d\'ajout d\'outils ');

     meta.addTags([
       { name: 'author',   content: 'ECE Paris'},
       { name: 'keywords', content: 'plateforme ARECE, dimensionnement énergie, outils dimensionnement d\'énergie'},
       { name: 'description', content: 'Formulaire d\'ajout d\'outils de dimensionnement d\'énergie qui peut être remplie par les utilisateurs de manière à avertir l\'administration de la plateforme' }
     ]);
     this.tool = new Tool(-1,'','','','','','','');
     this.form = fb.group({
       toolname:['',Validators.required],
       type:['',Validators.required],
       username:['',Validators.required],
       mail:['',Validators.compose([Validators.required,Validators.email])],
       link:['',Validators.required],
       descritpion:['',Validators.required]
     });
   }

   public onSubmit(){
        this.toolService.addDemandTool(this.tool)
            .subscribe(
            success => this.success=true,
            error =>
              this.snackBar.open("Il y a eu un soucis lors de l'envoi du formulaire. Veuillez réessayer.", "Fermer", {
                duration: 5000,
              }),
            ()=>
              this.snackBar.open("La demande d'ajout d'outil a bien été prise en compte", "Fermer", {
                duration: 5000,
              })
            );
   }
}

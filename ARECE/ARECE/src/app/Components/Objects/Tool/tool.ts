import { Component } from '@angular/core';

import {Jsonp} from '@angular/http';


export class Tool {
    public id:number;
    public userName:string;
    public toolName:string;
    public typeDemand:string;
    public etat:string;
    public link:string;
    public mail:string;
    public description:string;

  public static fromJson(json: Object): Tool {
     return new Tool(
       json['idTool'],
       json['userName'],
       json['toolname'],
       json['typeDemand'],
       json['status'],
       json['link'],
       json['mail'],
       json['description']
     );
 }

 constructor( id:number,
   userName:string,
   toolName:string,
   typeDemand:string,
   etat:string,
   link:string,
   mail:string,
   description:string) {
     this.id=id;
     this.userName=userName;
     this.toolName=toolName;
     this.typeDemand=typeDemand;
     this.etat=etat;
     this.link=link;
     this.mail=mail;
     this.description=description;

   }

}

import { Component,Input, OnInit, Injectable,ChangeDetectorRef,ViewChild } from '@angular/core';
import {Tool} from '../../Objects/Tool/tool';
import {Http, Headers, Response, Jsonp, RequestOptions} from '@angular/http';
import {MatPaginator} from '@angular/material';

import { DataSource } from '@angular/cdk/table';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';


@Injectable()
export class ToolService {

    private toolsUrl = 'http://plateforme.arece.fr/php/tools/';

    constructor(private http: Http) {}

    public getDemandTools(): Observable<Tool[]> {
      const headers = new Headers({
        'content-type': 'application/json'
      });

        return this.http.get(this.toolsUrl+'getDemandTools.php',{ headers: headers }).map((response: Response): Tool[] => {
                return response.json().map((json: Object) => Tool.fromJson(json));
            });
    }

    public getTools(): Observable<Tool[]> {
      const headers = new Headers({
        'content-type': 'application/json'
      });

        return this.http.get(this.toolsUrl+'getTools.php',{ headers: headers }).map((response: Response): Tool[] => {
                return response.json().map((json: Object) => Tool.fromJson(json));
            });
    }
    public getToolById(id:number): Observable<Tool> {
      return this.getTools().map(tools =>tools.find(tool =>tool.id ==id));
    }


    public addDemandTool(tool : Tool): Observable<Object> {

      let body = JSON.stringify({tool});
      let headers = new Headers({ 'Content-Type':  'application/json; charset=utf-8'});
      let options = new RequestOptions({ headers: headers });

      return this.http.post(this.toolsUrl+'addDemandTool.php', body, options)
      .catch((e: any) => Observable.throw(this.handleError(e)));
    }
    public deleteDemandTool(tool : Tool): Observable<Object> {

      let body = JSON.stringify({tool});
      let headers = new Headers({ 'Content-Type':  'application/json; charset=utf-8'});
      let options = new RequestOptions({ headers: headers });

      return this.http.post(this.toolsUrl+'deleteDemandTool.php', body, options)
      .catch((e: any) => Observable.throw(this.handleError(e)));
    }

    public addTool(tool : Tool): Observable<Object> {

      let body = JSON.stringify({tool});
      let headers = new Headers({ 'Content-Type':  'application/json; charset=utf-8'});
      let options = new RequestOptions({ headers: headers });

      return this.http.post(this.toolsUrl+'addTool.php', body, options)
      .catch((e: any) => Observable.throw(this.handleError(e)));
    }

    private handleError(error: any) {
        console.error(error);
     }

     private extractData(res: Response) {
      let body = res.json();
      return body || {}; // remove 'fields'
    }
}

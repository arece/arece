import { Utilisateur } from '../../Objects/Utilisateur/utilisateur';

import { Component,Input, OnInit, Injectable,ChangeDetectorRef,ViewChild } from '@angular/core';
import {Http, Headers, Response, Jsonp, RequestOptions} from '@angular/http';
import {MatPaginator} from '@angular/material';

import { DataSource } from '@angular/cdk/table';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';

@Injectable()
export class UtilisateurService {

  constructor(private http: Http) { }

  private connexionUrl = "http://localhost/arece/ARECE/ARECE/php/connect.php";
  //
  // add(mail: string, motdepasse : string): void {
  //   mail = mail.trim();
  //   if (!mail) { return; }
  //   motdepasse = motdepasse.trim();
  //   if (!motdepasse) { return; }
  //   this.sendUtilisateur(mail,motdepasse as Utilisateur)
  //     .subscribe(utilisateur => {
  //       this.utilisateurs.push(utilisateur);
  // });
  // }

  public sendUtilisateur (utilisateur: Utilisateur): Observable<Response> {

    let body = JSON.stringify({utilisateur});
    let headers = new Headers({ 'Content-Type': 'application/json; charset=utf-8'});
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.connexionUrl, body, options)
    .catch(this.handleError);
    }
  // public putData() {
  //   let headers = new Headers({
  //   'content-type': 'application/json'
  //   });
  //
  //     headers.append('Content-Type', 'application/json; charset=utf-8');
  //     headers.append('Accept', 'application/json; charset=utf-8');
  //
  //     this.http.put(this.connexionUrl,utilisateur,httpOptions)
  //       .subscribe(result => {
  //           this.data = result;
  //           console.dir(this.data);
  //       });
  //
  // }


      private handleError(error: Response) {
         console.error(error);
         return Observable.throw(error.json()|| 'Server Error');
       }

       private extractData(res: Response) {
        let body = res.json();
        return body || {}; // remove 'fields'
      }
}

import { Component } from '@angular/core';

import {Jsonp} from '@angular/http';

@Component({
  selector: 'app-utilisateur',
  templateUrl: './utilisateur.html',
  styleUrls: ['./utilisateur.css']
})

export class Utilisateur {
    nom: string;
    prenom: string;
    mail: string;
    motdepasse: string;

    constructor( nom:string,
      prenom:string,
      mail:string,
      motdepasse:string,
      ) {
        this.nom=nom;
        this.prenom=prenom;
        this.mail=mail;
        this.motdepasse=motdepasse;
      }
      public setLocalStorage(){
         localStorage.setItem('currentUser', 'connect');
         console.log(localStorage.getItem('currentUser'));
      }

}

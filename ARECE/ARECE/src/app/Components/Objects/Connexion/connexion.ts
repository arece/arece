import { Component, ViewChild,PLATFORM_ID, Inject, OnInit } from '@angular/core';
import { isPlatformServer, isPlatformBrowser } from '@angular/common';
import {UtilisateurService} from '../../Objects/Utilisateur/utilisateur.service';
import {Utilisateur} from '../../Objects/Utilisateur/utilisateur';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { Meta, Title} from '@angular/platform-browser';
import {Router } from '@angular/router';


@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.html',
  styleUrls: ['./connexion.css'],
  providers: [UtilisateurService]
})

export class Connexion implements OnInit {
    title = 'Connexion';
    utilisateur : Utilisateur;
    form: FormGroup;
    token:string;
    mail: string;
    motdepasse: string;
    errorMessage: any;
    user1: any;


    constructor(@Inject(FormBuilder) fb: FormBuilder, private utilisateurService: UtilisateurService, meta: Meta, title: Title,private router:Router) {
      title.setTitle('Connexion administrateur');

      meta.addTags([
        { name: 'author',   content: 'ECE Paris'},
        { name: 'keywords', content: 'plateforme ARECE, dimensionnement énergie, connexion administrateur'},
        { name: 'description', content: 'Page de connexion dédiée à l\'administration pour gérer les différentes demandes des utilisateurs' }
      ]);
      this.utilisateur = new Utilisateur("","",this.mail,this.motdepasse);
      this.form = fb.group({
        mail:['',Validators.compose([Validators.required,Validators.email])],
        motdepasse:['',Validators.required],
      });


    }

    // public ngOnInit(){
    //   this.form = new FormGroup({
    //     'mail': new FormControl(this.mail,Validators.required),
    //     'motdepasse': new FormControl(this.motdepasse, Validators.required)
    //   });
    // }

    public onSubmit(){

         this.utilisateurService.sendUtilisateur(this.utilisateur)
             .subscribe(
             res =>
            this.token=res.text(),
             error => this.errorMessage = <any>error);
         if(this.token === 'ERREUR'){
           this.utilisateur.setLocalStorage();
           this.router.navigateByUrl('/administration-arece');
         }
    }
    ngOnInit() {
      //  if (isPlatformServer(platformId)) {
      //     console.log("log serv");
      // }
      //
      //  if (isPlatformBrowser(platformId)) {
      //    console.log(this.user1);       }
   }
}

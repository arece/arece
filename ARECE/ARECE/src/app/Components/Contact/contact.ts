import { Component, ViewChild, OnInit } from '@angular/core';
import { Meta, Title} from '@angular/platform-browser';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.html',
  styleUrls: ['./contact.css']
})
export class Contact{
  constructor(meta: Meta, title: Title) {

  title.setTitle('Contact ARECE');

  meta.addTags([
    { name: 'author',   content: 'ECE Paris'},
    { name: 'keywords', content: 'plateforme ARECE, dimensionnement énergie, contact'},
    { name: 'description', content: 'Les informations nécessaires pour contacter l\'un des acteurs de la plateforme ARECE' }
  ]);
}
}

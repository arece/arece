import { Component, ViewChild } from '@angular/core';
import { Meta, Title} from '@angular/platform-browser';


@Component({
  selector: 'app-diagnosol',
  templateUrl: './diagnosol.html',
  styleUrls: ['./diagnosol.css']
})
export class Diagnosol {
  title = 'header';

  constructor(meta: Meta, title: Title) {

  title.setTitle('Outil Diagnosol');

  meta.addTags([
    { name: 'author',   content: 'ECE Paris'},
    { name: 'keywords', content: 'plateforme ARECE, dimensionnement énergie, Diagnosol'},
    { name: 'description', content: '' }
  ]);}
}

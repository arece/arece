import { Component, ViewChild, OnInit } from '@angular/core';
import { Meta, Title} from '@angular/platform-browser';


@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.html',
  styleUrls: ['./accueil.css']
})
export class Accueil implements OnInit{
  constructor(meta: Meta, title: Title) {

  title.setTitle('Accueil Arece');

  meta.addTags([
    { name: 'author',   content: 'ECE Paris'},
    { name: 'keywords', content: 'plateforme ARECE, dimensionnement énergie'},
    { name: 'description', content: 'La plateforme ARECE propose des outils de dimensionnement d\'énergie, son but est de créer une communauté d\'entraide et de discussion sur des sujets liés au dimmensionnement d\'énergie' }
  ]);

}
  ngOnInit(){

  }

}

import { Component, ViewChild } from '@angular/core';
import { Meta, Title} from '@angular/platform-browser';


@Component({
  selector: 'app-calsimeol',
  templateUrl: './calsimeol.html',
  styleUrls: ['./calsimeol.css']
})
export class Calsimeol {
  title = 'header';

  constructor(meta: Meta, title: Title) {
  title.setTitle('Outil Calsimeol');

  meta.addTags([
    { name: 'author',   content: 'ECE Paris'},
    { name: 'keywords', content: 'plateforme ARECE, dimensionnement énergie, Calsimeol'},
    { name: 'description', content: '' }
  ]);
}
}

import { Component,Input, OnInit, Injectable,ChangeDetectorRef,ViewChild } from '@angular/core';
import {Tool} from '../../Objects/Tool/tool';
import {ToolService} from '../../Objects/Tool/toolservice';
import {Http, Headers, Response, Jsonp} from '@angular/http';
import {MatPaginator} from '@angular/material';

import { DataSource } from '@angular/cdk/table';
import {MatSnackBar} from '@angular/material';
import { Meta, Title} from '@angular/platform-browser';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';


@Component({
  selector: 'app-toolsList',
  templateUrl: './toolsList.html',
  styleUrls: ['./toolsList.css'],
  providers: [ToolService]
})
export class ToolsList implements OnInit {
  displayedColumns = ['id','etat','toolName' ,'typeDemand' , 'link'];
  dataSource:ToolDataSource | null;
  errorMessage: any;
  success:boolean;

  @Input()
  tools:Tool[];
  selectedTool:Tool;



  constructor(private http: Http,private toolService:ToolService,private changeDetector: ChangeDetectorRef, public snackBar:MatSnackBar,meta: Meta, title: Title) {

    title.setTitle('Liste des demandes');

    meta.addTags([
      { name: 'author',   content: 'ECE Paris'},
      { name: 'keywords', content: 'plateforme ARECE, dimensionnement énergie, administration'},
      { name: 'description', content: 'La liste des différentes demandes d\'ajout d\'outils en cours validées' }
    ]);

   }

  ngOnInit() {
    this.toolService.getDemandTools().subscribe((tools: Tool[]) => {
        this.tools = tools;
    },
    // Errors will call this callback instead:
    err => {
      console.log('Something went wrong!');
    });

    this.dataSource= new ToolDataSource(this.toolService);
    this.changeDetector.detectChanges();
  }

  onSelect(tool: Tool): void {
    this.selectedTool = tool;
  }
    public initComponent(){
        this.refreshList();
    }
    public refreshList(){
      this.dataSource=new ToolDataSource(this.toolService);
      this.selectedTool=null;
    }

    public addTool(){
      this.toolService.addTool(this.selectedTool).subscribe(
        success =>
         this.selectedTool.etat="Acceptée"
          ,
        error =>
          this.snackBar.open("Il y a eu un soucis lors de l'ajout. Veuillez réessayer.", "Fermer", {
            duration: 5000,
          }),
        ()=>
          this.snackBar.open("L'outil a bien été ajouté à la plateforme.", "Fermer", {
            duration: 5000,
          })
        );

    }

    public deleteDemandTool(){
      this.toolService.deleteDemandTool(this.selectedTool).subscribe(
        success => this.success=true ,
        error =>
          this.snackBar.open("Il y a eu un soucis lors de la suppression. Veuillez réessayer.", "Fermer", {
            duration: 5000,
          }),
        ()=>
          this.snackBar.open("La demande a bien été refusée.", "Fermer", {
            duration: 5000,
          })
        );

        this.tools.splice(this.tools.indexOf(this.selectedTool),1);
        this.refreshList();
    }



}

export class ToolDataSource extends DataSource<Tool> {

  constructor(private toolService:ToolService) {
    super();
  }

  connect(): Observable<Tool[]> {
    return this.toolService.getDemandTools();
  }

	disconnect() { }
}

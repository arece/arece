  import { Component, ViewChild, OnInit, Injectable } from '@angular/core';
import {Http, Headers, Response, Jsonp} from '@angular/http';
import { Meta, Title} from '@angular/platform-browser';
import { CanActivate, Router, CanActivateChild } from '@angular/router';

import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
@Component({
  selector: 'app-administration',
  templateUrl: './administration.html',
  styleUrls: ['./administration.css']
})
export class Administration{
  title = 'Administration';
  public data:any;

  public constructor(private http: Http, meta: Meta, title: Title, private router:Router) {

      title.setTitle('Administration d\'ajout d\'outils');

      meta.addTags([
        { name: 'author',   content: 'ECE Paris'},
        { name: 'keywords', content: 'plateforme ARECE, dimensionnement énergie, administration'},
        { name: 'description', content: 'L\'administration des demandes d\'ajout d\'outils se fait via cette page qui centralise les demandes' }
      ]);
    }
    public logOut(){
      localStorage.setItem('currentUser', 'null');
      console.log(localStorage.getItem('currentUser'));
      this.router.navigateByUrl('/accueil-arece');
    }
}

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild{
  public constructor(private router:Router) {
  }
  canActivate(){
    if (localStorage.getItem('currentUser') == "connect")  {
    console.log('penis');
    console.log(localStorage.getItem('currentUser'));
    return true; // all fine
  } else {
    this.router.navigate(["/connexion-admin-arece"]);}
  }

  canActivateChild(){
    if (localStorage.getItem('currentUser') == 'connect')  {
      console.log('penis enfant');
    return true; // all fine
  } else {
    this.router.navigate(["/connexion-admin-arece"]);}
  }


}

import { Component, ViewChild } from '@angular/core';
import { Meta, Title} from '@angular/platform-browser';


@Component({
  selector: 'app-about',
  templateUrl: './about.html',
  styleUrls: ['./about.css']
})
export class About {
  constructor(meta: Meta, title: Title) {

  title.setTitle('À propos d\'Arece');

  meta.addTags([
    { name: 'author',   content: 'ECE Paris'},
    { name: 'keywords', content: 'plateforme ARECE, dimensionnement énergie'},
    { name: 'description', content: 'Brief définition de ce qu\'est la plateforme ARECE, de son histoire et de ses objectifs en terme de collaboration en dimensionnement d\'énergie' }
  ]);

}
}

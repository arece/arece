import { Component,OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { Meta, Title} from '@angular/platform-browser';


import {Tool} from '../Objects/Tool/tool';
import {ToolService} from '../Objects/Tool/toolservice';

@Component({
  selector: 'tool-view',
  templateUrl: './tool-view.html',
  styleUrls: ['./tool-view.css'],
  providers:[ToolService]
})
export class ToolView implements OnInit {
  url:any;

  constructor(private toolService:ToolService,private route:ActivatedRoute,private sanitizer: DomSanitizer,meta: Meta, title: Title){
    title.setTitle('Outil dimensionnement');

    meta.addTags([
      { name: 'author',   content: 'ECE Paris'},
      { name: 'keywords', content: 'plateforme ARECE, dimensionnement énergie, Forum énergie'},
      { name: 'description', content: 'Les outils de dimensionnement sont disponibles gratuitement à n\'importe quel utilisateur, chacun peut les utiliser à leur guise à des fins professionnelles ou personnelles' }
    ]);}

  ngOnInit() {
    this.route.params.forEach((params:Params)=>{
      let id = +params['id'];
      this.getUrl(id);
    });
  }
  getUrl(id:number){
    this.toolService.getToolById(id).subscribe((tool: Tool) => {
        this.url = this.getSafeUrl(tool.link);
    },
    // Errors will call this callback instead:
    err => {
      console.log('Something went wrong!');
    });
  }
  getSafeUrl(url) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(url)
  }



}
